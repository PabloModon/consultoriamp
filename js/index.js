$(function () {
          $("[data-toggle='tooltip']").tooltip();
          $("[data-toggle='popover']").popover();
          $('.carousel').carousel({
              interval: 5000
          })
          $('#informe1').on('show.bs.modal', function(e){
            console.log('el modal se está mostrando');
            $('#informeBtn1').removeClass('btn-info');
            $('#informeBtn1').addClass('btn-outline-success w-100');
            $('#informeBtn1').prop('disabled', true);

          });
          $('#informe1').on('shown.bs.modal', function(e){
            console.log('el modal se mostró');

          });
          $('#informe1').on('hide.bs.modal', function(e){
            console.log('el modal se está ocultando');
            $('#informeBtn1').removeClass('btn-outline-success w-100');
            $('#informeBtn1').addClass('btn-info');
            $('#informeBtn1').prop('disabled', false);


          });
          $('#informe1').on('hidden.bs.modal', function(e){
            console.log('el modal se ocultó');

          });
      })